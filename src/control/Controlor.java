package control;
import model.*;
import view.*;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;



public class Controlor {
	
	private Gui theV;
	
	public Controlor (Gui view)
	{
			this.theV= view;
			this.theV.addCalcListener(new addCalcListenerC());
			this.theV.subCalcListener(new subCalcListenerC());
			this.theV.inmuCalcListener(new inmCalcListenerC());
			this.theV.impCalcListener(new impCalcListenerC());
			this.theV.derivezFirstCalcListener(new derivfCalcListenerC());
			this.theV.derivezSecondCalcListener(new derivsCalcListenerC());
			this.theV.integrezFirstCalcListener(new integfCalcListenerC());
			this.theV.integrezSecondCalcListener(new integsCalcListenerC());
	}
	

	class addCalcListenerC implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Operatie operatie=new Operatie();
			String stringPol1="0",stringPol2="0";
			 
				 stringPol1=theV.getFirstPol();
				 stringPol2=theV.getSecPol();
				 Polinom polinomul1 = new Polinom(stringPol1);
				 Polinom polinomul2 = new Polinom(stringPol2);
				 Polinom rezult =operatie.add(polinomul1,polinomul2,true);
				 theV.setRez(rezult.toString());
		}
	}
	class subCalcListenerC implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Operatie operatie=new Operatie();
			String stringPol1="0X",stringPol2="0X";
			 
				 stringPol1=theV.getFirstPol();
				 stringPol2=theV.getSecPol();
				 Polinom polinomul1 = new Polinom(stringPol1);
				 Polinom polinomul2 = new Polinom(stringPol2);
				 Polinom rezult =operatie.add(polinomul1,polinomul2,false);
				 theV.setRez(rezult.toString());
		}
	}
	class inmCalcListenerC implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Operatie operatie=new Operatie();
			String stringPol1="0",stringPol2="0";
			 
				 stringPol1=theV.getFirstPol();
				 stringPol2=theV.getSecPol();
				 Polinom polinomul1 = new Polinom(stringPol1);
				 Polinom polinomul2 = new Polinom(stringPol2);
				 Polinom rezult =operatie.inmultire(polinomul1,polinomul2);
				 theV.setRez(rezult.toString());
		}
	}
	class impCalcListenerC implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Operatie operatie=new Operatie();
			String stringPol1="0",stringPol2="0";
			 
				 stringPol1=theV.getFirstPol();
				 stringPol2=theV.getSecPol();
				 Polinom polinomul1 = new Polinom(stringPol1);
				 Polinom polinomul2 = new Polinom(stringPol2);
				 Polinom rezult =operatie.impartire(polinomul1,polinomul2);
				 theV.setRez(rezult.toString());
		}
	}
	class derivfCalcListenerC implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Operatie operatie=new Operatie();
			String stringPol1="0";
				 stringPol1=theV.getFirstPol();
				 Polinom polinomul = new Polinom(stringPol1);
				 Polinom subRezult =operatie.deriv(polinomul);
				 theV.setRez1(subRezult.toString());
		}
	}
		class derivsCalcListenerC implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				Operatie operatie=new Operatie();
				String stringPol="0";
					 stringPol=theV.getSecPol();
					 Polinom polinomul = new Polinom(stringPol);
					 Polinom subRezult =operatie.deriv(polinomul);
					 theV.setRez2(subRezult.toString());
			}
	}
		class integfCalcListenerC implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				Operatie operatie=new Operatie();
				String stringPol="0";
					 stringPol=theV.getFirstPol();
					 Polinom polinomul = new Polinom(stringPol);
					 Polinom subRezult =operatie.integ(polinomul);
					 theV.setRez1(subRezult.toString());
			}
	}
		class integsCalcListenerC implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				Operatie operatie=new Operatie();
				String stringPol="0";
					 stringPol=theV.getSecPol();
					 Polinom polinomul = new Polinom(stringPol);
					 Polinom subRezult =operatie.integ(polinomul);
					 theV.setRez2(subRezult.toString());
			}
	}
}

	
	

//x^5+3x^3-2x^2-3x^1+6
//-x^5-6