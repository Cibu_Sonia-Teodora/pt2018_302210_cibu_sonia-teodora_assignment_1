package view;


import java.awt.event.ActionListener;


import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.*;
import javax.swing.*;
public class Gui extends JFrame {
	private JButton adunPol = new JButton ("Adunare");
	private JButton scadPol = new JButton ("Scadere");
	private JButton inmultescPol = new JButton ("Imultire");
	private JButton impartPol = new JButton ("Impatire");
	private JButton integrezPol1 = new JButton ("Integreaza");
	private JButton integrezPol2 = new JButton ("Integreaza");
	private JButton derivezPol1 = new JButton ("Deriveaza");
	private JButton derivezPol2 = new JButton ("Deriveaza");
	private JTextField polinom1 = new JTextField("",15);
	private JTextField polinom2 = new JTextField("",15);
	private JTextField polinomRez1 = new JTextField("",15);
	private JTextField polinomRez2 = new JTextField("",15);
	private JTextField polinomRez = new JTextField("",15);
	private JLabel pol1 = new JLabel("Polinomul 1");
	private JLabel pol2 = new JLabel("Polinomul 2");
	private JLabel polR = new JLabel("Rezultat");
	private JLabel polR1 = new JLabel("Rezultat 1");
	private JLabel polR2 = new JLabel("Rezultat 2");
	
	
	public Gui ()
	{
		
		JPanel content = new JPanel();
		JPanel left = new JPanel();
		JPanel center = new JPanel();
		JPanel right = new JPanel();
		
		left.setLayout(new BoxLayout(left,BoxLayout.Y_AXIS));
		left.add(Box.createRigidArea(new Dimension(0, 5)));
		left.add(pol1);
		left.add(Box.createRigidArea(new Dimension(0, 5)));
		left.add(polinom1);
		left.add(Box.createRigidArea(new Dimension(0, 5)));
		left.add(integrezPol1);
		left.add(Box.createRigidArea(new Dimension(0, 5)));
		left.add(derivezPol1);
		left.add(Box.createRigidArea(new Dimension(0, 5)));
		left.add(polR1);
		left.add(Box.createRigidArea(new Dimension(0, 5)));
		left.add(polinomRez1);
		left.add(Box.createRigidArea(new Dimension(0, 5)));
		
		
		center.setLayout(new BoxLayout(center,BoxLayout.Y_AXIS));
		center.add(Box.createRigidArea(new Dimension(0, 5)));
		center.add(pol2);
		center.add(Box.createRigidArea(new Dimension(0, 5)));
		center.add(polinom2);
		center.add(Box.createRigidArea(new Dimension(0, 5)));
		center.add(integrezPol2);
		center.add(Box.createRigidArea(new Dimension(0, 5)));
		center.add(derivezPol2);
		center.add(Box.createRigidArea(new Dimension(0, 5)));
		center.add(polR2);
		center.add(Box.createRigidArea(new Dimension(0, 5)));
		center.add(polinomRez2);
		center.add(Box.createRigidArea(new Dimension(0, 5)));
		
		right.setLayout(new BoxLayout(right,BoxLayout.Y_AXIS));
		right.add(Box.createRigidArea(new Dimension(0, 5)));
		right.add(polR);
		right.add(Box.createRigidArea(new Dimension(0, 5)));
		right.add(polinomRez);
		right.add(Box.createRigidArea(new Dimension(0, 5)));
		right.add(adunPol);
		right.add(Box.createRigidArea(new Dimension(0, 5)));
		right.add(scadPol);
		right.add(Box.createRigidArea(new Dimension(0, 5)));
		right.add(inmultescPol);
		right.add(Box.createRigidArea(new Dimension(0, 5)));
		right.add(impartPol);
		right.add(Box.createRigidArea(new Dimension(0, 5)));
		
		polinomRez1.setEditable(false);
		polinomRez2.setEditable(false);
		polinomRez.setEditable(false);
		
		content.setLayout(new BoxLayout(content,BoxLayout.X_AXIS));
		content.add(left);
		content.add(center);
		content.add(right);
		
		this.setContentPane(content);
		this.pack();
		this.setTitle("Operatii pe Polinoame");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		
	}
	public String getFirstPol()
	{
		return polinom1.getText();
	}
	public String getSecPol()
	{
		return polinom2.getText();
	}
	public void setRez1(String rez)
	{
		polinomRez1.setText(rez);
	}
	public void setRez2(String rez)
	{
		polinomRez2.setText(rez);
	}
	public void setRez(String rez)
	{
		polinomRez.setText(rez);
	}
	 public void addCalcListener(ActionListener addButton)
			 {
		 adunPol.addActionListener(addButton);
			 }
	 public void subCalcListener(ActionListener subButton)
	 		{
		 scadPol.addActionListener(subButton);
	 		}
	 public void inmuCalcListener(ActionListener inmuButton)
		{
	 inmultescPol.addActionListener(inmuButton);
		}
	 public void impCalcListener(ActionListener impButton)
		{
	 impartPol.addActionListener(impButton);
		}
	 public void integrezFirstCalcListener(ActionListener integFButton)
		{
	 integrezPol1.addActionListener(integFButton);
		}
	 public void integrezSecondCalcListener(ActionListener integSButton)
		{
	 integrezPol2.addActionListener(integSButton);
		}
	 public void derivezFirstCalcListener(ActionListener derivezFButton)
		{
	 derivezPol1.addActionListener(derivezFButton);
		}
	 public void derivezSecondCalcListener(ActionListener derivezSButton)
		{
	 derivezPol2.addActionListener(derivezSButton);
		}

}
