package model;

import java.util.ArrayList;
import java.util.List;

public class Operatie {
	

    public Polinom add(Polinom polinom1, Polinom polinom2, boolean sum) {
    	// daca boolean e true atunci fac suma 
    	// daca boolean e false atunci fac diferenta
        ArrayList<Monom> rezult = new ArrayList<Monom>();
        ArrayList<Monom> p1 = polinom1.getArrayL();
        ArrayList<Monom> p2 = polinom2.getArrayL();
        int i = 0;
        int j = 0;

        //verific daca am date in cele doua polinoame
        if (polinom1.getArrayL().size() == 0 && polinom2.getArrayL().size() != 0) {
            return polinom2;
        }

        if (polinom1.getArrayL().size() != 0 && polinom2.getArrayL().size() == 0) {
            return polinom1;
        }

        while (i < p1.size() && j < p2.size()) {

            if (p1.get(i).getGrad() == p2.get(j).getGrad()) {
                if (sum) {
                    rezult.add(new Monom(p1.get(i).getGrad(), p1.get(i).getCoef() + p2.get(j).getCoef()));
                } else {
                    rezult.add(new Monom(p1.get(i).getGrad(), p1.get(i).getCoef() - p2.get(j).getCoef()));
                }
                i++;
                j++;

            } else {
            	// verific gradele monoamelor daca-s diferite il add pe cel cu grad max si trec la urmatoul
                if (p1.get(i).getGrad() > p2.get(j).getGrad()) {
                    rezult.add(new Monom(p1.get(i).getGrad(), p1.get(i).getCoef()));
                    i++;
                } else {

                    if (p1.get(i).getGrad() < p2.get(j).getGrad()) {
                        if (sum) {
                            rezult.add(new Monom(p2.get(j).getGrad(), p2.get(j).getCoef()));
                        } else {
                            rezult.add(new Monom(p2.get(j).getGrad(), -p2.get(j).getCoef()));
                        }
                        j++;
                    }
                }
            }

        }
        //verific daca mai sunt monoame ramase care nu au fost parcurse anterior si le add in rez
        if (i < p1.size()) {
            while (i < p1.size()) {
                rezult.add(new Monom(p1.get(i).getGrad(), p1.get(i).getCoef()));
                i++;
            }
        }

        if (j < p2.size()) {
            while (j < p2.size()) {
                if (sum) {
                    rezult.add(new Monom(p2.get(j).getGrad(), p2.get(j).getCoef()));
                } else {
                    rezult.add(new Monom(p2.get(j).getGrad(), -p2.get(j).getCoef()));
                }
                j++;
            }
        }

        for (i = 0; i < rezult.size(); i++) {
            if (rezult.get(i).getCoef() == 0) {
                rezult.remove(i);
                i--;
            }
        }

        return new Polinom(rezult);
    }
    public Polinom inmultire(Polinom polinom1, Polinom polinom2) {
    	Operatie oper=new Operatie();
        List<Polinom> rezultat = new ArrayList<>();// creez liste de monoame care rep rezultatul polinom1 imultit cu cate un monom sin polinom 2
        for (int i = 0; i < polinom1.getArrayL().size(); i++) {
            ArrayList<Monom> rezult = new ArrayList<>();
            for (int j = 0; j < polinom2.getArrayL().size(); j++) {
                Monom monom = new Monom();
                monom.setCoef(polinom2.getArrayL().get(j).getCoef() * polinom1.getArrayL().get(i).getCoef());
                monom.setGrad(polinom2.getArrayL().get(j).getGrad() + polinom1.getArrayL().get(i).getGrad());
                rezult.add(monom);
            }
            rezultat.add(new Polinom(rezult));// add rezultatele partiale
        }
        Polinom finalResult = new Polinom();
        for (int i = 0; i < rezultat.size(); i++) {
            Polinom partResult = finalResult;
            finalResult = oper.add(partResult, rezultat.get(i), true);//adun listele partiale de polinome rezultate
        }
        return finalResult;
    }

    
    public Polinom deriv(Polinom polinom)
    {
    	ArrayList<Monom> rez = new ArrayList<Monom>();
    	for (Monom mon:polinom.getArrayL())
    	{
    		rez.add(mon.derivez());
    	}
    	return new Polinom(rez);
    }
    
    public Polinom integ(Polinom polinom )
    {
    	ArrayList<Monom> rez = new ArrayList<Monom>();
    	for (Monom mon:polinom.getArrayL())
    	{
    		rez.add(mon.integrez());
    	}
    	return new Polinom(rez);
    }
 
    public Polinom impartire(Polinom polinom1,Polinom polinom2){
    	Operatie oper=new Operatie();
        ArrayList<Monom> catPol = new ArrayList<>();
       // if(polinom1.getArrayL().size()<polinom2.getArrayL().size()){
        //vezi ce faci aici
    //}
        //algoritmu matematic de impartire a doua polinoame
        while(polinom1.getArrayL().get(0).getGrad()>=polinom2.getArrayL().get(0).getGrad()) {
            Monom monom = new Monom(polinom1.getArrayL().get(0).getGrad()
                    - polinom2.getArrayL().get(0).getGrad(),
                    polinom1.getArrayL().get(0).getCoef() /
                            polinom2.getArrayL().get(0).getCoef());// monomul rez din impartirea monoamelor cu grade max
            catPol.add(monom);
            ArrayList<Monom> monoms = new ArrayList<>();
            monoms.add(monom);
            Polinom polinom = new Polinom(monoms);
            Polinom newPolinom = new Polinom();
            newPolinom = oper.inmultire(polinom2, polinom);//inmultirea catului partial cu deimp
            polinom1 = oper.add(polinom1, newPolinom, false);//scaderea din impartitor 

            if(polinom1.getArrayL().size()==0){
                break;
            }
        }
       // CATUL IMPARTIRII care este si returnat 
        //System.out.println(new Polinom(catPol));
      // RESTUL IMPARTIRII pe care am preferat sa nu il returnez 
        //System.out.println(polinom1);
        return new Polinom(catPol);//catul
    }
   
}
