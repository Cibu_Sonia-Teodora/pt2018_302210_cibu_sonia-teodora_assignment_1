package model;

public class Monom  {
	private int grad;
	private Double coef;
	public Monom(int grad, double coef)
	{
		this.grad=grad;
		this.coef=coef;
	}
	public Monom(){}
	
	public  int getGrad()
	{
		return this.grad;
	}
	public  double getCoef()
	{
		return this.coef;
	}
	public void setGrad(int grad)
	{
		this.grad=grad;
	}
	public void setCoef(double coef)
	{
		this.coef=coef;
	}
	public  Monom derivez()
	{
		if (grad==0)
			return new Monom(0,0.0);
		return new Monom (this.grad-1, this.grad *this.coef);
	}
	public  Monom  integrez()
	{
		 if(this.coef==0) return new Monom(0,0.0);
		return new Monom (this.grad+1, this.coef*(1/(double)(this.grad+1)));
	}
	public String toString()
	{
		int j=coef.intValue();
		if (coef==0) return "";
		if (grad==0)
		{
			if(coef==0)return "0";
			if (coef>0)
			{
			if(j==coef)
				return "+"+j+"";
			else return "+"+coef+"";
			}
			else 
			{
				if(j==coef)
					return j+"";
				else return coef+"";
			}
		}
		else
		{
			if(coef>=0)
			{
			if (j==coef)
				return "+"+j+"X^"+grad;
			else return "+"+coef+"X^"+grad;
			}
			else 
			{
				if (j==coef)
					return j+"X^"+grad;
				else return coef+"X^"+grad;
			}
		}
		
	}
				
	}

