package model;


import java.util.*;

public class Polinom {

    private ArrayList<Monom> p;

    public Polinom() {
        p = new ArrayList<Monom>();
    }

    public Polinom(ArrayList<Monom> in) {
        this.p = in;
    }

    public Polinom(String str) {
    	p = new ArrayList<Monom>();
    	if (str.length()==0)p.add(new Monom(0,0.0));
        int currentMonom = 0;
        String[] arr = str.split("\\^");//faci escape pt caracter ^

        for (int i = 0; i < arr.length; ++i)//parcurgi fiecare substring
        {
            char ch = arr[i].charAt(0);//salvezi primu caracter din substring
            if (ch == 'X')//daca e x inseamna ca e doar la inceput si are coef 1
            {
                p.add(new Monom(0, 1));
            }
            if (isSign(ch))//daca e  + sau -, inseamna ca e la inceput si coef e sign*coef
            {
                double number = 0;
                int poz = arr[i].indexOf('X');
                if (poz == 1)
                    number = 1;//daca e ceva de genu -x^2 sau +x^3
                else {
                    if (poz == -1)
                        poz = arr[i].length();
                    String value = arr[i].substring(1, poz);
                    number = Double.parseDouble(value);


                }
                if (ch == '-')
                    number *= -1;//schimbi semnu daca e necesar
                p.add(new Monom(0, number));
            } else if (isDigit(ch)) {
                int poz = arr[i].indexOf('+');
                if (poz == -1)
                    poz = arr[i].indexOf('-');

                int pozx = arr[i].indexOf('X');

                if (pozx == -1 && poz == -1)//daca e x^25
                {
                    String value = arr[i].substring(0, arr[i].length());//in conditie normala, scoti substringu de pana la sign
                    int number = Integer.parseInt(value);//obtii gradul pt monomul precedent
                    double tempcoef = p.get(currentMonom).getCoef();
                    p.set(currentMonom, new Monom(number, tempcoef));//adaugi grad pt monomu precedent
                    continue;
                }
                if (pozx > -1 && poz == -1)//daca la inceput e 56x^9
                {
                    String value = arr[i].substring(0, pozx);
                    Double coef = Double.parseDouble(value);
                    p.add(new Monom(0, coef));
                    continue;//nu mai executi ce urmeaza si treci la urmatoru ciclu for
                }

                if (pozx == -1 && poz > -1)//x^5+63
                {
                    String value = arr[i].substring(0, poz);//in conditie normala, scoti substringu de pana la sign
                    int number = Integer.parseInt(value);//obtii gradul pt monomul precedent
                    double tempcoef = p.get(currentMonom).getCoef();
                    p.set(currentMonom, new Monom(number, tempcoef));//adaugi grad pt monomu precedent

                    value = arr[i].substring(poz + 1, arr[i].length());//practic scoti pe 63
                    double coef = Double.parseDouble(value);//il faci double
                    if (arr[i].charAt(poz) == '-')
                        coef *= -1;//schimbi semnu
                    p.add(currentMonom, new Monom(0, coef));//adaugi ultimu monom
                    continue;
                }
                //daca e 25+6x^
                String value = arr[i].substring(0, poz);//in conditie normala, scoti substringu de pana la sign
                int number = Integer.parseInt(value);//obtii gradul pt monomul precedent
                double tempcoef = p.get(currentMonom).getCoef();
                p.set(currentMonom, new Monom(number, tempcoef));//adaugi grad pt monomu precedent

                ++currentMonom;//ai terminat cu precedentu, treci la urmtoru
                value = arr[i].substring(poz + 1, pozx);//scoti coef
                double coef = Double.parseDouble(value);//il faci double
                if (arr[i].charAt(poz) == '-')
                    coef *= -1;//schimbi semnu
                p.add(currentMonom, new Monom(0, coef));//adaugi ultimu monom
            }
        }
        System.out.println(p);
    }



    public ArrayList<Monom> getArrayL() {
        return p;
    }

    public String toString() {
        String s = "";
        for (Monom mon : this.p) {
            s = s + mon.toString();
        }

        if (p.size() == 0) {
            return "0";
        }

        System.out.println(s);
        return s;
    }

    public void setArrayL(ArrayList<Monom> pol) {
        this.p = pol;
    }

    static boolean isSign(char ch) {
        if (ch == '+' || ch == '-')
            return true;
        else
            return false;
    }

    static boolean isDigit(char ch) {
        if (ch >= '0' && ch <= '9')
            return true;
        else
            return false;
    }
}